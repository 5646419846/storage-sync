package net.seajay.storagesync;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class StorageSync {
//    public static String[] allowed_extensions = {"mp4"};

    public static void main(String[] args) {
        ArrayList<File> syncDrivers = new ArrayList<>();
        for (File disk : File.listRoots()) {
            System.out.println("Detected disks : " + disk.getAbsolutePath());
            try {
                FileStore store = Files.getFileStore(disk.toPath());
                System.out.println("available=" + store.getUsableSpace() + ", total=" + store.getTotalSpace());
            } catch (IOException e) {
                System.out.println("error querying space: " + e.toString());
            }

            if (new File(disk.getAbsolutePath()+ ".meta/isStorageSync").isFile()) {
                syncDrivers.add(disk);
            }

        }

        for (File disk : syncDrivers) {
            System.out.println("Sync enabled for drive \"" + disk.getAbsolutePath() + "\"");
        }
        ArrayList<File> masterDrivers = new ArrayList<>();
        System.out.println("Search master driver...");
        for (File disk : syncDrivers) {
            if (new File(disk.getAbsolutePath()+ ".meta/isMaster").isFile()) {
                masterDrivers.add(disk);
                System.out.println("Found \"" + disk.getAbsolutePath() + ".meta/isMaster\"");
            }
        }
        if (masterDrivers.size() > 1) {
            System.out.println("Found > 1 master sync drivers. Sync aborted.");
            System.out.println("[FAILED]");
            return;
        }
        File master = masterDrivers.get(0);
        System.out.println("Sync from \""+ FileSystemView.getFileSystemView().getSystemDisplayName(master) + "\" [" + master.getAbsolutePath() + "]");

        HashMap<File, String> masterFilesHashes = getDriveFilesHashes(master);

        for (File file : masterFilesHashes.keySet()) {
            System.out.println(masterFilesHashes.get(file) + " | " + file.getAbsolutePath());
        }

        for (File drive : syncDrivers) {
            if (!drive.getAbsolutePath().equals(master.getAbsolutePath())){
                syncDrive(masterFilesHashes, drive);
            }
        }
        for (File drive:syncDrivers) {
            try {
                //здесь "sleep 15" и есть ваша консольная команда
                Process proc = Runtime.getRuntime().exec("explorer " + drive.getAbsolutePath());
                proc.waitFor();
                proc.destroy();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

    }



    private static String createSha1(File file) throws Exception  {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        InputStream fis = new FileInputStream(file);
        int n = 0;
        byte[] buffer = new byte[8192];
        while (n != -1) {
            n = fis.read(buffer);
            if (n > 0) {
                digest.update(buffer, 0, n);
            }
        }
        fis.close();
        return bytesToHex(digest.digest());
    }
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static void syncDrive(HashMap<File, String> masterHashes, File targetDrive) {
        System.out.println("Sync to " + FileSystemView.getFileSystemView().getSystemDisplayName(targetDrive) + "\" [" + targetDrive.getAbsolutePath() + "]");
        HashMap<File, String> driveHashes = getDriveFilesHashes(targetDrive);

        ArrayList<File> targetDriveAddQueue = new ArrayList<>();
        for (File file: masterHashes.keySet()) {
            String masterHash = masterHashes.get(file);
            if (!driveHashes.containsValue(masterHash)) {
                targetDriveAddQueue.add(file);
            }
        }

        ArrayList<File> targetDriveRemoveQueue = new ArrayList<>();
        for (File file: driveHashes.keySet()) {
            String hash = driveHashes.get(file);
            if (!masterHashes.containsValue(hash)) {
                targetDriveRemoveQueue.add(file);
            }
        }

        int s = targetDriveRemoveQueue.size();
        int i = 0;
        for (File file : targetDriveRemoveQueue) {
            System.out.println("(" + ++i + "/" + s + ") " + "Delete " + file.getAbsolutePath());
            file.delete();
        }
        s = targetDriveAddQueue.size();
        i = 0;
        for (File file : targetDriveAddQueue) {
//            System.out.println("(" + ++i + "/" + s + ") " + "Copy " + file.getAbsolutePath() + " to drive " + "\""+ targetDrive.getAbsolutePath() +"\"" + FileSystemView.getFileSystemView().getSystemDisplayName(targetDrive));
            System.out.println("(" + ++i + "/" + s + ") " + "Copy " + file.getAbsolutePath() + " to drive " + FileSystemView.getFileSystemView().getSystemDisplayName(targetDrive));
            try {
                copyFile(file, new File(targetDrive.getAbsolutePath() + file.getName()));
            } catch (IOException e) {
                System.out.println("COPY ERROR " + file.getAbsolutePath());
                System.out.println("[FAILED]");
                e.printStackTrace();
            }

        }

        System.out.println("End sync to " + "\""+ targetDrive.getAbsolutePath() +"\"" + FileSystemView.getFileSystemView().getSystemDisplayName(targetDrive));
        System.out.println(targetDriveAddQueue.size() + " files coped, " + targetDriveRemoveQueue.size() + " files removed.");


    }
    private static HashMap<File, String> getDriveFilesHashes(File drive) {
        HashMap<File, String> masterFilesHashes = new HashMap<>();

        for (File file : Objects.requireNonNull(drive.listFiles())) {
            if (file.isFile()) {
                try {
                    masterFilesHashes.put(file, createSha1(file));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("This is no file - " + file.getAbsolutePath());
            }
        }
        return masterFilesHashes;
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
    }

}
